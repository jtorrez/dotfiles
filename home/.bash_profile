# AWS Keys
export AWS_CREDENTIALS_FILE=$HOME/.aws/credentials.csv
export AWS_ACCESS_KEY_ID=$(tail -n 1 $AWS_CREDENTIALS_FILE | cut -f 2 -d ",")
export AWS_SECRET_ACCESS_KEY=$(tail -n 1 $AWS_CREDENTIALS_FILE | cut -f 3 -d ",")

#Adding Go env variables
export GOPATH=$HOME/go
export GOBIN=$GOPATH/bin
export PATH=$PATH:$GOPATH/bin

#Setting Spark Home Directory
export SPARK_HOME=`brew info apache-spark | grep /usr | cut -f 1 -d " "`/libexec
export PYTHONPATH=$SPARK_HOME/python:$PYTHONPATH

#Setting Hadoop Home Directory
export HADOOP_HOME=`brew info hadoop | grep /usr | cut -f 1 -d " "`/libexec
export LD_LIBRARY_PATH=$HADOOP_HOME/lib/native/:$LD_LIBRARY_PATH

# Setting Tor password
export TOR_FILE=$HOME/.tor_pass
export TOR_PASS=$(head -n 1 $TOR_FILE)

# Setting api-key directory
export API_KEY_DIR=$HOME/.api-keys

# Marvel API keys
export MARVEL_PUB_KEY=$(head -n 1 $API_KEY_DIR/marvel_keys.csv | cut -f 1 -d ",")
export MARVEL_PRI_KEY=$(head -n 1 $API_KEY_DIR/marvel_keys.csv | cut -f 2 -d ",")

# Aliases
alias c='clear'
alias h='history'
alias l='ls -F'
alias ll='ls -lGFh'
alias lla='ls -lGFhA'
alias jp='jupyter notebook'
alias enderjp='ssh -NL localhost:8889:localhost:8889 ender'
alias godowork='GOPATH=$(realpath ../../../..)'
alias purgeme='sudo purge'
alias mem='du -hs'
alias mypep8='pep8 --max-line-length=99'
alias d='docker'
alias dc='docker-compose'
alias dsa='docker stop $(docker ps -qa)'
alias dka='docker kill $(docker ps -qa)'
alias drma='docker rm $(docker ps -qa)'

# added by Anaconda2 4.0.0 installer
export PATH="/Users/jtorrez/anaconda2/bin:$PATH"

# Allow access to custom shell scripts
export PATH="/Users/jtorrez/bin:$PATH"

# Make sure brew installed apps work
export PATH="/usr/local/sbin:$PATH"
export PATH="/usr/local/bin:$PATH"

# Add git tab completion
source ~/git-completion.bash

# Run .bashrc
source ~/.bashrc

# Start py35 conda environment
source activate py35

# Added by Rust installation
export PATH="$HOME/.cargo/bin:$PATH"

# iterm2 theme: https://github.com/stoeffel/material-iterm
# purple time & date | turquoise user red AT turquose host | green cwd | yellow git branch | no color
export PS1="\[\033[01;35m\][\d \A] \[\033[1;36m\]\u\[\033[1;31m\]@\[\033[1;36m\]\h \[\033[1;32m\]\w \[\033[01;33m\](\$(git branch 2>/dev/null | grep '^*' | colrm 1 2)) \[\033[00m\]\$ "
