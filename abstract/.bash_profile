#Adding Go env variables
export GOPATH=$HOME/go
export GOBIN=$GOPATH/bin
export PATH=$PATH:$GOPATH/bin

# Aliases
alias c='clear'
alias h='history'
alias l='ls -F'
alias ll='ls -lGFh'
alias lla='ls -lGFhA'
alias jp='jupyter notebook'
alias remotejp='ssh -NL localhost:8889:localhost:8889 ender'
alias purgeme='sudo purge'
alias mem='du -hs'
alias d='docker'
alias dc='docker-compose'
alias dsa='docker stop $(docker ps -qa)'
alias dka='docker kill $(docker ps -qa)'
alias drma='docker rm $(docker ps -qa)'
alias cdwd='cd ~/go/src/github.com/goabstract/projects/'
alias pip='pip3'
alias python='python3'

# Make sure brew installed apps work
export PATH=/usr/local/bin:/usr/local/sbin:$PATH

# Added by Rust installer
export PATH="$HOME/.cargo/bin:$PATH"

# Added to support Ruby install
export PATH="$HOME/.rbenv/bin:$PATH"
eval "$(rbenv init -)"

# Init jenv
if which jenv > /dev/null; then eval "$(jenv init -)"; fi

# git completion
# curl https://raw.githubusercontent.com/git/git/master/contrib/completion/git-completion.bash -o ~/.git-completion.bash
if [ -f ~/.git-completion.bash ]; then . ~/.git-completion.bash; fi

# iterm2 theme: https://github.com/stoeffel/material-iterm
# red time & date | purple cwd | turquoise git branch | no color
# export PS1="\[\033[1;31m\][\d \A]  \[\033[1;35m\]\w \[\033[01;36m\](\$(git branch 2>/dev/null | grep '^*' | colrm 1 2)) \[\033[00m\]\$ "

# light blue time & date | golden cwd | dark pink git branch | no color (true triadic)
# export PS1="\[\033[38;5;39m\][\d \A]  \[\033[38;5;220m\]\w \[\033[38;5;197m\](\$(git branch 2>/dev/null | grep '^*' | colrm 1 2)) \[\033[00m\]\$ "

# light turquoise time & date | golden cwd | dark pink git branch | no color (true triadic)
export PS1="\[\033[38;5;45m\][\d \A]  \[\033[38;5;220m\]\w \[\033[38;5;197m\](\$(git branch 2>/dev/null | grep '^*' | colrm 1 2)) \[\033[00m\]\$ "

# light turquoise time & date | golden cwd | dark pink git branch | purple command | no color (triadic/tetradic mix)
# export PS1="\[\033[38;5;45m\][\d \A]  \[\033[38;5;220m\]\w \[\033[38;5;197m\](\$(git branch 2>/dev/null | grep '^*' | colrm 1 2)) \[\033[38;5;93m\]\$ \[\033[00m\]"

# light turquoise time & date | orange cwd | golden git branch | purple command | no color (true tetradic)
# export PS1="\[\033[38;5;45m\][\d \A]  \[\033[38;5;208m\]\w \[\033[38;5;220m\](\$(git branch 2>/dev/null | grep '^*' | colrm 1 2)) \[\033[38;5;93m\]\$ \[\033[00m\]"

# Source .bashrc to get tokens and secrets
source ~/.bashrc
