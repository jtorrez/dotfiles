set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'
" Go dev  plugin
Plugin 'fatih/vim-go'
" Fuzzy finder
Plugin 'ctrlpvim/ctrlp.vim'
" Struct splitter and joiner
Plugin 'AndrewRadev/splitjoin.vim'
" Git wrapper plugin
Plugin 'tpope/vim-fugitive'
" Dope colorscheme
Plugin 'NLKNguyen/papercolor-theme'
" highlight that python syntax
Plugin 'hdima/python-syntax'
" Python syntax, style, and complexity checker
Plugin 'nvie/vim-flake8'
" airline status bar 
Plugin 'vim-airline'
" directory tree plugins
Plugin 'scrooloose/nerdtree'
Plugin 'Xuyuanp/nerdtree-git-plugin'
" auto-complete plugin
Plugin 'Valloric/YouCompleteMe'
" code commenting
Plugin 'tpope/vim-commentary'
" Rust file detection, syntax highlighting, formatting, etc.
Plugin 'rust-lang/rust.vim'
" Rust code completion and navigation
Plugin 'racer-rust/vim-racer'
" JS syntax highlighting
Plugin 'pangloss/vim-javascript'
Plugin 'mxw/vim-jsx'
" Async linting
Plugin 'w0rp/ale'
" ack Plugin
Plugin 'mileszs/ack.vim'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
au FileType html setl sw=2 sts=2 et
au FileType javascript,json setl sw=2 sts=2 et

syntax on

set number
set hidden
set autowrite
set encoding=utf-8
set backspace=indent,eol,start
set splitbelow
set splitright
set laststatus=2

" Make it easy to find whitespace
set listchars=eol:¬,tab:>·,trail:~,extends:>,precedes:<,space:␣

" Color scheme
set background=dark
colorscheme PaperColor
let python_highlight_builtins = 1

let g:PaperColor_Theme_Options = {
  \   'language': {
  \     'python': {
  \       'highlight_builtins' : 1
  \     },
  \   }
  \ }

" define my leader mapping
let mapleader = ","

" vim-go settings
let g:go_test_timeout = '10s'

" NERDTree settings
" open NERDTree automatically if opening a directory
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif

" YouCompleteMe settings
let g:ycm_autoclose_preview_window_after_completion=1

" make ack.vim plugin use Ag in the backend instead of ack if available
if executable('ag')
  let g:ackprg = 'ag --nogroup --nocolor --column'
endif

" emmet settings
let g:user_emmet_leader_key='<Tab>'
let g:user_emmet_settings = {
  \  'javascript.jsx' : {
    \      'extends' : 'jsx',
    \  },
  \}

" mapping to automatically take you to definition of variable
map <leader>g  :YcmCompleter GoToDefinitionElseDeclaration<CR>

" mapping to make moving through quickview faster
map <C-n> :cnext<CR>
map <C-m> :cprevious<CR>
nnoremap <leader>c :cclose<CR>

" commands to make opening alternate go files sensible
autocmd Filetype go command! -bang A call go#alternate#Switch(<bang>0, 'edit')
autocmd Filetype go command! -bang AV call go#alternate#Switch(<bang>0, 'vsplit')
autocmd Filetype go command! -bang AS call go#alternate#Switch(<bang>0, 'split')

" vim-go mappings
autocmd FileType go nmap <leader>r <Plug>(go-run)
autocmd FileType go nmap <leader>t <Plug>(go-test)
autocmd FileType go nmap <leader>c <Plug>(go-coverage-toggle)
autocmd FileType go nmap <leader>i <Plug>(go-info)

" run :GoBuild or :GoTestCompile based on the go file
function! s:build_go_files()
  let l:file = expand('%')
  if l:file =~# '^\f\+_test\.go$'
    call go#cmd#Test(0, 1)
  elseif l:file =~# '^\f\+\.go$'
    call go#cmd#Build(0)
  endif
endfunction

" mapping to build the Go file with appropriate command
autocmd FileType go nmap <leader>b :<C-u>call <SID>build_go_files()<CR>

" mapping to toggle NERDTree
map <leader>n :NERDTreeToggle<CR>

" mapping to run flake8 tool
autocmd FileType python map <buffer> <leader>8  :call Flake8()<CR>

" mapping to rapidly toggle whitespace viewing
nmap <leader>l :set list!<CR>

" general mappings
inoremap <leader>a <Esc>
inoremap <leader>i <Esc>
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

