#Adding Go env variables
export GOPATH=$HOME/go
export GOBIN=$GOPATH/bin
export PATH=$PATH:$GOPATH/bin

# Aliases
alias c='clear'
alias h='history'
alias l='ls -F'
alias ll='ls -lGFh'
alias lla='ls -lGFhA'
alias jp='jupyter notebook'
alias remotejp='ssh -NL localhost:8889:localhost:8889 ender'
alias purgeme='sudo purge'
alias mem='du -hs'
alias d='docker'
alias dc='docker-compose'
alias dsa='docker stop $(docker ps -qa)'
alias dka='docker kill $(docker ps -qa)'
alias drma='docker rm $(docker ps -qa)'
alias sdes='docker run -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" docker.elastic.co/elasticsearch/elasticsearch:6.4.3'

# Make sure brew installed apps work
export PATH=/usr/local/bin:/usr/local/sbin:$PATH

# Add git tab completion
source ~/git-completion.bash

# Added by Rust installer
export PATH="$HOME/.cargo/bin:$PATH"

# Init jenv
if which jenv > /dev/null; then eval "$(jenv init -)"; fi

# iterm2 theme: https://github.com/stoeffel/material-iterm
# purple time & date | turquoise user red AT turquose host | green cwd | yellow git branch | no color
export PS1="\[\033[01;35m\][\d \A] \[\033[1;36m\]\u\[\033[1;31m\]@\[\033[1;36m\]\h \[\033[1;32m\]\w \[\033[01;33m\](\$(git branch 2>/dev/null | grep '^*' | colrm 1 2)) \[\033[00m\]\$ "
